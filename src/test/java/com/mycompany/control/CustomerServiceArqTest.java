package com.mycompany.control;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.URL;
import java.util.Date;
import java.util.Locale;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.mycompany.entity.Company;
import com.mycompany.entity.Customer;
import com.mycompany.entity.Sex;
import com.mycompany.testutils.DeploymentDescriptor;

@RunWith(Arquillian.class)
public class CustomerServiceArqTest {

	@ArquillianResource
	private URL contextPath;

	@EJB
	private CustomerService customerService;

	@EJB
	private CompanyService companyService;

	@Deployment(testable = true, order = 1, name = "crmDemoWebArchive", managed = true)
	public static WebArchive createDeployment() {
		return DeploymentDescriptor.createDeployment();
	}

	@Test // @Before besser?
	@InSequence(1)
	public void testSaveCustomer() throws ValidationException {
		Company company = new Company("GISA GmbH");
		companyService.saveCompany(company);
		
		Customer customer = new Customer("Max", "Mustermann", 
				"max@mustermann.de", "0123456789", "01234561234",
				Sex.male, "Germany", Locale.GERMANY, new Date(), 
				companyService.listAllCompanies().get(0));
		
		customerService.saveCustomer(customer);
	}

	@Test
	@InSequence(2)
	public void testFindAllCustomers() {
		assertEquals(1, customerService.findAllCustomers().size());
	}
	
	@Test
	@InSequence(3)
	public void testFindCustomerById() {		
		Customer customer = customerService.findCustomerById(1l);
		assertEquals("Max", customer.getFirstName());
		assertEquals("Mustermann", customer.getLastName());
	}
	
	@Test
	@InSequence(4)
	public void testFindCustomers() {		
		assertEquals(1, customerService.findCustomers("Mustermann").size());
	}
	
	@Test
	@InSequence(5)
	public void testUpdateCustomer() throws ValidationException {
		Customer customer = customerService.findCustomerById(1l);
		customer.setFirstName("Meike");
		customer.setSex(Sex.female);
		
		customerService.updateCustomer(customer);
		
		Customer test_customer = customerService.findCustomerById(1l);
		assertEquals("Meike", test_customer.getFirstName());
		assertEquals(Sex.female, test_customer.getSex());
	}
	
	@Test
	@InSequence(6)
	public void testDeleteCustomer(){		
		customerService.deleteCustomer(1l);
		assertEquals(0, customerService.findAllCustomers().size());
	}

}
